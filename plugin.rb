# name: discourse-ci-sso
# about: CI SSO support for Discourse
# version: 0.1
# authors: CI

auth_provider :title => 'with CI',
              :authenticator => Auth::OpenIdAuthenticator.new('ci','http://ci.u493.black.elastictech.org/openid/provider', trusted: true),
              :message => 'Authenticating with CI (make sure pop up blockers are not enabled)',
              :frame_width => 1000,   # the frame size used for the pop up window, overrides default
              :frame_height => 800

register_css <<CSS

.btn-social.ci {
  background: #468c2c;
}

.btn-social.ci:before {
  content: "C";
}

CSS
